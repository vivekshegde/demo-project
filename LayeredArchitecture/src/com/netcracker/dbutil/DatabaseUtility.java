package com.netcracker.dbutil;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.netcracker.exceptions.DatabaseException;


public class DatabaseUtility {

	static Connection con;
	public static Connection getDbConnection() throws DatabaseException {
		
		try {
		FileInputStream fis = new FileInputStream("jdbc.properties");
		Properties props = new Properties();
		props.load(fis);
		
		con = DriverManager.getConnection(props.getProperty("url"),props.getProperty("username"),props.getProperty("password"));
		}catch(Exception e) {
			
			throw new DatabaseException("Database connectivity exception"+e.getMessage());
		}
		return con;
	}
	
	public static void closeDbConnection() throws DatabaseException{
		
		try {
			
			con.close();
		}catch(SQLException e) {
			
			throw new DatabaseException(e.getMessage());
		}
	}

}
