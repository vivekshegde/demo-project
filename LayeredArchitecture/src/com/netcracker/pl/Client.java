package com.netcracker.pl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.netcracker.dto.Account;
import com.netcracker.exceptions.AccountException;
import com.netcracker.exceptions.DatabaseException;
import com.netcracker.service.AccountService;
import com.netcracker.service.AccountServiceImpl;

public class Client {

	public static void main(String[] args) {
		
		System.out.println("1: Account opening");
		System.out.println("2: Account Info Display");
		System.out.println("3: get all account");
		System.out.println("4: delete account");
		System.out.println("5: exit");

		AccountService accountService = new AccountServiceImpl();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			
			System.out.println("enter the choice");
			
			try {
				int choice = Integer.parseInt(br.readLine());
				
				switch(choice) {
				
				case 1: {
					
					System.out.println("enter the account holder name");
					String holderName = br.readLine();
					System.out.println("enter the account type");
					String accountType = br.readLine();
					System.out.println("enter the account balance");
					float balance =Float.parseFloat(br.readLine());
					Account account = new Account();
					account.setAccName(holderName);
					account.setAccType(accountType);
					account.setAccBal(balance);
					
					String result = accountService.saveAccountData(account);
					System.out.println(result);
						break;
				}
				case 2 : {
					
					System.out.println("enter the account number");
					int accNumber = Integer.parseInt(br.readLine());
					Account accountDetails = accountService.getDetailsByAccountNumber(accNumber);
					System.out.println(accountDetails);
					break;
				}
				
				case 3 : {
					
					System.out.println("All the accounts");
				List<Account> allAccount = accountService.getAllAccount();
				for(Account a:allAccount) {
					System.out.println(a);
				}
				}
				break;
				case 4 : {
				
					System.out.println("enter the acc no to be deleted");
					int accNo = Integer.parseInt(br.readLine());
					String result = accountService.deleteAccount(accNo);
					System.out.println(result);
					break;
				}
				
				case 5 :{
					
					System.exit(0);
				}
				
				default :
					
					System.out.println("please enter valid case");
				
				}
				break;
			} catch (DatabaseException | AccountException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch( NumberFormatException e) {
				
				System.out.println("please enter the valid number");
			}
		}

	}

}
