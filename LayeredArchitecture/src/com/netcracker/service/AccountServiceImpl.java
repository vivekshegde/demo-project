package com.netcracker.service;

import java.util.List;

import com.netcracker.dao.AccountDAO;
import com.netcracker.dao.AccountDAOImpl;
import com.netcracker.dto.Account;
import com.netcracker.exceptions.AccountException;
import com.netcracker.exceptions.DatabaseException;

public class AccountServiceImpl implements AccountService {

	AccountDAO accountDao = new AccountDAOImpl();
	
	@Override
	public String saveAccountData(Account account) throws AccountException, DatabaseException {
		
		
		
		return accountDao.saveAccountData(account);
	}

	@Override
	public Account getDetailsByAccountNumber(int accountNumber) throws AccountException, DatabaseException {
		
		return accountDao.getDetailsByAccountNumber(accountNumber);
	}

	@Override
	public List<Account> getAllAccount() throws AccountException, DatabaseException {
		
		return accountDao.getAllAccount();
	}

	@Override
	public String deleteAccount(int accountNumber) throws AccountException, DatabaseException {
		
		return accountDao.deleteAccount(accountNumber);
	}

}
