package com.netcracker.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.netcracker.dbutil.DatabaseUtility;
import com.netcracker.dto.Account;
import com.netcracker.exceptions.AccountException;
import com.netcracker.exceptions.DatabaseException;

public class AccountDAOImpl implements AccountDAO{
	

	@Override
	public String saveAccountData(Account account) throws AccountException, DatabaseException {
		
		String message=null;
		String query = "insert into account values(sq_name.nextval,?,?,?)";
		Connection con = DatabaseUtility.getDbConnection();
		
		try {
			
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1,account.getAccName());
			ps.setString(2,account.getAccType());
			ps.setFloat(3,account.getAccBal());
			int rowcount = ps.executeUpdate();
			
			if(rowcount==1) {
				
				message="Account is created";
			}
		}catch(SQLException e) {
			
			throw new AccountException("Account saving Exception "+e.getMessage());
		}
		DatabaseUtility.closeDbConnection();
		return message;
	}

	@Override
	public Account getDetailsByAccountNumber(int accountNumber) throws AccountException, DatabaseException {
		
		Account account = new Account();
		String query = "select *from account where acc_no=?";
		Connection con = DatabaseUtility.getDbConnection();
		
		try {
			PreparedStatement ps = con.prepareStatement(query);
			ps.setInt(1, accountNumber);
			 ResultSet result = ps.executeQuery();
			if( result.next()) { 
				 account.setAccountNumber(result.getInt(1));
				 account.setAccName(result.getString(2));
				 account.setAccType(result.getString(3));
				 account.setAccBal(result.getInt(4));
			}
		}catch(SQLException e) {
			throw new AccountException("AccountDetails Exception "+e.getMessage());
		}
			
		return account;
	}

	@Override
	public List<Account> getAllAccount() throws AccountException, DatabaseException {
		
		
		List<Account> list = new ArrayList<Account>();
		String query = "select *from account";
		Connection con = DatabaseUtility.getDbConnection();
		try {
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet result = ps.executeQuery();
			while(result.next()) {
				Account account = new Account();
				account.setAccountNumber(result.getInt(1));
				account.setAccName(result.getString(2));
				account.setAccType(result.getString(3));
				account.setAccBal(result.getFloat(4));
				list.add(account);
			}
			
			
		}catch(SQLException e) {
			throw new AccountException("AccountDetails Exception "+e.getMessage());
		}
		
		
		return list;
	}

	@Override
	public String deleteAccount(int accountNumber) throws AccountException, DatabaseException {
		
		Account account = new Account();
		String message = null;
		String query = "delete from account where acc_no=?";
		Connection con = DatabaseUtility.getDbConnection();
		
		try {
			
			PreparedStatement ps = con.prepareStatement(query);
			ps.setInt(1, accountNumber);
			int rowCount = ps.executeUpdate();
			if(rowCount==1) {
				message = "Given "+accountNumber+" is successfully deleted";
			}else
				message = "invalid accountNumber";
			
		}catch(SQLException e) {
			
			throw new AccountException("Account Deletion "+e.getMessage());
		}
		return message;
	}
	
	

}
