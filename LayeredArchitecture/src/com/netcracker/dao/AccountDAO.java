package com.netcracker.dao;

import java.util.List;

import com.netcracker.dto.Account;
import com.netcracker.exceptions.AccountException;
import com.netcracker.exceptions.DatabaseException;

public interface AccountDAO {
	
	public String saveAccountData(Account account) throws AccountException, DatabaseException;
	
	public Account getDetailsByAccountNumber(int accountNumber) throws AccountException, DatabaseException;
	
	public List<Account> getAllAccount() throws AccountException, DatabaseException;
	
	public String deleteAccount(int accountNumber) throws AccountException, DatabaseException;

}
