package com.netcracker.dto;

public class Account {
	
	private int accountNumber;
	private String accName;
	private float accBal;
	private String accType;
	
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Account(int accountNumber, String accName, float accBal, String accType) {
		super();
		this.accountNumber = accountNumber;
		this.accName = accName;
		this.accBal = accBal;
		this.accType = accType;
	}
	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", accName=" + accName + ", accBal=" + accBal + ", accType="
				+ accType + "]";
	}
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	public float getAccBal() {
		return accBal;
	}
	public void setAccBal(float accBal) {
		this.accBal = accBal;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	
	
}
