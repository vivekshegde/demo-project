package com.netcracker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.netcracker.dto.Emp;

public class ListEx {

	List l;
	
	public void createArrayList() {
		
		List l = new ArrayList();
		l.add("Max");
		l.add(1);
		l.add(1.22);
		l.add('a');
		l.add(2, "Nitin");
		
		System.out.println("contents are "+l);
		
		//System.out.println("size is "+l.size());
		//System.out.println(l.get(2));
		if(l.contains("Max")) {
			l.remove(0);
			System.out.println(l);
		}
		//System.out.println(l);
		
		Iterator it = l.iterator();
		while(it.hasNext()) {   //like infinite loop
			System.out.println(it.next());  //iterate one by one
			it.remove(); //removes the elements from the list
		}
		
		System.out.println("size is "+l.size());
				
	}
	
	
	public void createEmpList() {
		
		List<Emp> empList = new ArrayList<Emp>();
		Emp e = new Emp(10,"raj",1000);
		Emp e1 = new Emp(11,"ram",2000);
		Emp e2 = new Emp(12,"rav",3000);
		empList.add(e);
		empList.add(e1);
		empList.add(e2);
		
		for(Emp o:empList) {
		System.out.println(o);
		}
		if(e.getId()==10) {
			System.out.println(e);
		}
	}
	
	public void createLinkList() {
		
		List<Integer> account = new LinkedList<Integer>();
		account.add(1);
		account.add(3);
		account.add(2);
		
		System.out.println(account.get(2));
	}
	
}
