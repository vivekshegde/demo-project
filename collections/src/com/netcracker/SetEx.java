package com.netcracker;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import com.netcracker.dto.Emp;

public class SetEx {
	
	//set is not index based structure
	Set<Integer> mySet;
	
	public void createHashSet() {
		
		//Explicitly pass the object of comparator to make use of it
		mySet = new TreeSet<Integer>(new MySortingLog());  //custom ordering has been implemented
		mySet.add(118);
		mySet.add(11);
		mySet.add(110);
		mySet.add(200);
		mySet.add(167);
		mySet.add(195);
		
		
		System.out.println("size is :"+ mySet.size());
		
		System.out.println("contents are");
		for(int i:mySet) {
			System.out.println(i);
		}
	}
	
	public void createEmpSet() {
		
		Set<Emp> empSet = new TreeSet<Emp>(); //ByDefault it implements for comparable 
		Emp e = new Emp(10,"raj",1000);
		Emp e1 = new Emp(11,"ram",2000);
		Emp e2 = new Emp(12,"rav",3000);
		empSet.add(e);
		empSet.add(e1);
		empSet.add(e2);
		
		System.out.println(empSet);
	}

}
