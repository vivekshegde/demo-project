package com.netcracker.dto;

public class Emp implements Comparable<Emp>{
	
	private int id;
	private String name;
	private float sal;
	
	public Emp() {
		super();
		
	}

	public Emp(int id, String name, float sal) {
		super();
		this.id = id;
		this.name = name;
		this.sal = sal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSal() {
		return sal;
	}

	public void setSal(float sal) {
		this.sal = sal;
	}

	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", sal=" + sal + "]";
	}

	@Override
	public int compareTo(Emp o) {
		
		return o.id-this.id;
	}
	
	

}
