package com.netcracker;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapEx {
	
	Map<Long,String> userList;
	
	public void createHashMap() {
		
		userList = new HashMap<Long,String>(); 
		userList.put(1212121212L, "Max");
		userList.put(1313131313L, "Marry");    //key must be unique
		//userList.put(1313131313L, "Marten");   ///overrides the previous value 
		userList.put(1000000000L, "Mortal");
		
		System.out.println(userList.size());
		System.out.println(userList.get(1000000000L));
		
		Set<Long> allKeys = userList.keySet();
		System.out.println(allKeys);
		
		for(Long key:allKeys) {
			System.out.println(key + " "+ userList.get(key));
		}
		
	}
	
	public void createTreeMap() {
		
		userList = new TreeMap<Long,String>(); 
		userList.put(1212121212L, "Max");
		userList.put(1313131313L, "Marry");    //key must be unique
		//userList.put(1313131313L, "Marten");   ///overrides the previous value 
		userList.put(1000000000L, "Mortal");
		
		System.out.println(userList.size());
		System.out.println(userList.get(1000000000L));
		
		Set<Long> allKeys = userList.keySet();
		System.out.println(allKeys);
		
		for(Long key:allKeys) {
			System.out.println(key + " "+ userList.get(key));
		}
	}

}
