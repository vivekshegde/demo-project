package index;
public class demo{
public static void main(String args[]){
System.out.println(new BTreeDemo().Start());
}
}

class BTreeDemo {

public int Start(){
Tree root;
boolean bool;


root = new Tree();
bool = root.Init(16);
bool = root.Print();
System.out.println(100);
bool = root.Print();
System.out.println("After Inserting data is! ");
bool = root.Insert(8);
bool = root.Insert(24);
bool = root.Insert(40);
bool = root.Insert(12);
bool = root.Insert(20);
bool = root.Insert(28);
bool = root.Insert(14);
bool = root.Insert(25);
bool = root.Insert(25);
bool = root.Print();

System.out.println("Search value is: " + root.Search(24));
System.out.println("Search value is: " + root.Search(12));
System.out.println("Search value is: " + root.Search(16));
System.out.println("Search value is: " + root.Search(50));
System.out.println("Search value is: " + root.Search(12));
System.out.println("Search value is: " + root.Search(25));
System.out.println("After deleting data! ");
bool = root.Delete(12);
bool = root.Delete(25);
bool = root.Print();
System.out.println(root.Search(12));
System.out.println("Delete value is: " + root.Delete(12));
System.out.println("Delete value is: " + root.Delete(25));
return 0 ;
}

}

class Tree{
Tree left ;
Tree right;
int key ;
boolean has_left ;
boolean has_right ;
Tree my_null ;

// Initialize a node with a key value and no children
public boolean Init(int v_key){
key = v_key ;
has_left = false ;
has_right = false ;
return true ;
}

public boolean Delete(int i) {
	// TODO Auto-generated method stub
	return false;
}

public String Search(int i) {
	// TODO Auto-generated method stub
	return null;
}

public boolean Insert(int i) {
	// TODO Auto-generated method stub
	return false;
}

public boolean Print() {
	// TODO Auto-generated method stub
	return false;
}

// Update the right child with rn
public boolean SetRight(Tree rn){
right = rn ;
return true ;
}

// Update the left child with ln
public boolean SetLeft(Tree ln){
left = ln ;
return true ;
}

public Tree GetRight(){
return right ;
}

public Tree GetLeft(){
return left;
}

public int GetKey(){
return key ;
}

public boolean SetKey(int v_key){
key = v_key ;
return true ;
}

public boolean GetHas_Right(){
return has_right ;
}

public boolean GetHas_Left(){
return has_left ;
}

public boolean SetHas_Left(boolean val){
has_left = val ;
return true ;
}

public boolean SetHas_Right(boolean val){
has_right = val ;
return true ;
}
}