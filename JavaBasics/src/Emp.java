
public class Emp {

	private int id;
	private String name;
	private float salary;  //instance variable
	
	static String companyName;   //class variable
	
	public Emp() { //default constructor
	
	}
	
	public Emp(int eid, String ename, float esalary) {
		
		id = eid;
		name = ename;
		salary = esalary;
	}
	
	public Emp(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public static String getCompanyName() {
		return companyName;
	}

	public static void setCompanyName(String companyName) {
		Emp.companyName = companyName;
	}

	public void calTotalSal() {
		
		
		int h = 5000;   //local variable
	}
	
	public static void calVarPay() {
		
		System.out.println("i am a part of class");
	}
	
	void showEmpData() {
		
		System.out.println(id);
		System.out.println(name);
		System.out.println(salary);
	}

	
	
	

}
