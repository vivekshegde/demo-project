import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Simp {
	
	int n;
	int y;
	void display() {
		System.out.println(n+" "+y);
	}
	
	@Override
	public String toString() {
		return "Simp [n=" + n + ", y=" + y + "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Simp s = new Simp();
		Simp s1 = new Simp();
		Simp s2 = new Simp();
		
		List<Simp> li = new ArrayList<Simp>();
		li.add(s);
		li.add(s1);
		li.add(s2);
		
		Iterator it = li.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}

}
