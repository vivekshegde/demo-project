
public class Parent {
	
	int page;

	public Parent(int page) {
		
		this.page = page;
		
		System.out.println("parent obj is created and page is "+ page);
	
	}
	
	void display() {
		
		System.out.println("parent age is "+page);
	}

}
