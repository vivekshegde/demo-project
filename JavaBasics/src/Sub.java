
public class Sub extends Parent {

	int cage;
	
	public Sub(int cage,int page) {
		
		super(page);
		this.cage = cage;
		
		System.out.println("sub object is created and cage is "+cage);
	}
	
	void display() {
		
		System.out.println("child age is "+cage);
	}
	
}
