package com.netcracker;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class FirstEx {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		
		//Class.forName("oracle.jdbc.driver.OracleDriver");
		System.out.println("driver is loaded");
		
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","oracle");
		System.out.println("connection is established");
		Statement st = con.createStatement();
		String insertQuery = "insert into employee values(122,'Max',10000)";
		int rowCnt = st.executeUpdate(insertQuery);
		System.out.println("no of rows inserted are "+ rowCnt);
		
		con.close();
	}
	

}
