package com.netcracker;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Login {
	
	public static void insertDetails(BufferedReader br,Connection con,PreparedStatement ps) throws IOException, SQLException {
		
		
		String query = "insert into login values(?,?)";
		ps = con.prepareStatement(query); 
		System.out.println("User details\n");
		System.out.println("enter the User Name");
		String name = br.readLine();
		System.out.println("enter the password");
		String pwd = br.readLine();
		ps.setString(1, name);
		ps.setString(2, pwd);
		ps.executeUpdate();
		System.out.println("details entered");
		
	}
	
	public static boolean loginAut(BufferedReader br,Connection con,PreparedStatement ps) throws IOException, SQLException {
		
		int flag=0,i=0;
		String stn[] = new String[4];
		System.out.println("enter the username to login");
		String name = br.readLine();
		String query = "select *from login";
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			//System.out.println("inside while loop");
			stn[i] = rs.getString(1);
			i++;
		}
		for(int j=0;j<stn.length;j++) {
		
			if(name.equals(stn[j])) {
				//System.out.println("inside if stat");
			flag=1;
			break;
		}else
			flag=0;
		}
	if(flag==1) {
		//System.out.println("inside flag if stat");
		return true;
		
	}else
		return false;
		
	}

	public static void main(String[] args) throws SQLException, IOException {
		
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","oracle");
		//System.out.println("connection is established");
		BufferedReader br = new BufferedReader(new java.io.InputStreamReader(System.in));
		PreparedStatement ps = null;
		System.out.println("enter yes/no/quit to give the login det");
		String ch = br.readLine();
		
		if(ch.equals("y")) {
			
		insertDetails(br,con,ps);
		}else if(ch.equals("n")) {
			
		 
		if(loginAut(br,con,ps)) {
			System.out.println("successfully login" );
		}else
			System.out.println("unsuccessfull login");
		}else {
		con.close();
		System.exit(0);
		}
	}

}
