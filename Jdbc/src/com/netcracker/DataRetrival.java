package com.netcracker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataRetrival {
	
public static void main(String[] args) throws ClassNotFoundException, SQLException {

		
		//Class.forName("oracle.jdbc.driver.OracleDriver");
		System.out.println("driver is loaded");
		
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","oracle");
		System.out.println("connection is established");
		Statement st = con.createStatement();
		String insertQuery = "select *from employee";
		ResultSet res = st.executeQuery(insertQuery);
		while(res.next()) {
			System.out.println(res.getInt(1)+" "+res.getString(2)+" "+res.getInt(3));
		}
//		int rowCnt = st.executeUpdate(insertQuery);
//		System.out.println("no of rows inserted are "+ rowCnt);
		con.close();
	}

}
