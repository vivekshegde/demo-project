package com.netcracker;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Sproc {

	public static void main(String[] args) throws SQLException {
		
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","oracle");
		System.out.println("connection is established");
		String qr = "{call getNameById(?,?)}";
		CallableStatement cs = con.prepareCall(qr);
		cs.setInt(1, 122);
		cs.registerOutParameter(2, java.sql.Types.VARCHAR);
		cs.execute();
		String name = cs.getString(2);
		System.out.println("name is "+name);
		con.close();
	}

}
