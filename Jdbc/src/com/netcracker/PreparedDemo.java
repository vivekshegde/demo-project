package com.netcracker;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class PreparedDemo {
	
public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

		
		//Class.forName("oracle.jdbc.driver.OracleDriver");
		System.out.println("driver is loaded");
		
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","oracle");
		System.out.println("connection is established");
		
		String query = "insert into employee values(?,?,?)";
		PreparedStatement p = con.prepareStatement(query);
		BufferedReader br = new BufferedReader(new java.io.InputStreamReader(System.in));
		
		while(true) {
		
			System.out.println("enter yes/no to give the emp details");
			String ch = br.readLine();
			
			if(ch.equals("yes")) {
				
			System.out.println("enter the emp_id ");
			int empid = Integer.parseInt(br.readLine());
			System.out.println("enter the emp_name ");
			String empname = br.readLine();
			System.out.println("enter the emp_salary ");
			int empsal = Integer.parseInt(br.readLine());
			
				p.setInt(1, empid);
				p.setString(2, empname);
				p.setInt(3, empsal);
				int c = p.executeUpdate();
				System.out.println("count is "+ c);
			}else {
			
				con.close();
				System.exit(0);
			}
		}
	}

}
