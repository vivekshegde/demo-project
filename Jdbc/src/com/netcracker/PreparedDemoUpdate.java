package com.netcracker;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class PreparedDemoUpdate {
	
public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

		
		//Class.forName("oracle.jdbc.driver.OracleDriver");
		System.out.println("driver is loaded");
		
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","oracle");
		System.out.println("connection is established");
		
		String query = "update employee set emp_salary=emp_salary+? where emp_id=?";
		PreparedStatement p = con.prepareStatement(query);
		BufferedReader br = new BufferedReader(new java.io.InputStreamReader(System.in));
		
		
		
			System.out.println("enter the update values");
			
			
			
				
			System.out.println("Updated emp_sal is  ");
			int empsal = Integer.parseInt(br.readLine());
			System.out.println("to which user ");
			int empid = Integer.parseInt(br.readLine());
			
				
			p.setInt(2, empid);
			p.setInt(1, empsal);		
				int c = p.executeUpdate();
				System.out.println("count is "+ c);
		
			
				con.close();
				
		
		}
	

}
