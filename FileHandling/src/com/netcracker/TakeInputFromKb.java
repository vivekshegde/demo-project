package com.netcracker;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TakeInputFromKb {

	public static void main(String[] args) throws IOException {

		BufferedInputStream bi = new BufferedInputStream(System.in);
		FileOutputStream fos = new FileOutputStream("adress.txt");
		
		System.out.println("enter address :");
		int c;
		while((c=bi.read())!='q') {
			
			fos.write(c);
		}
		System.out.println("address saved in file");
		
	}

}
