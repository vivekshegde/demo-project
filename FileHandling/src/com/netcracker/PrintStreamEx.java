package com.netcracker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class PrintStreamEx {

	public static void main(String[] args) throws IOException {

		FileOutputStream fo = new FileOutputStream("prnt.txt");
		PrintStream ps = new PrintStream(fo);
		ps.println("hi");
		 ps.close();
		 fo.close();
	}

}
