package com.netcracker;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileopyHand {
	
	public static void main(String[] args) throws IOException {
	
		FileInputStream fi = new FileInputStream("C:\\Users\\Vivek\\Desktop\\sachin.txt");
		FileOutputStream fo = new FileOutputStream("abc.txt",true);   //if abc.txt doesn't exist,then it will create of its own
		
		BufferedInputStream bi = new BufferedInputStream(fi);
		BufferedOutputStream bo = new BufferedOutputStream(fo);
		int c;
		while((c=bi.read())!=-1) {
			
			bo.write(c);
		
		}
		System.out.println("file is copied");
		bo.close();
		bi.close();
		fo.close();
		fi.close();
	}
	
	
	
}
