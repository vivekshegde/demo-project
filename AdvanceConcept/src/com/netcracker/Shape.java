package com.netcracker;

public abstract class Shape {
	
	int a,b;
	
	abstract public void calArea();
	
	public void displayShapeName() {
		System.out.println("shape name is unknown");
	}

	public Shape(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

}
