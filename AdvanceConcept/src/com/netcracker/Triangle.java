package com.netcracker;

public class Triangle extends Shape{
	
	

	public Triangle(int a, int b) {
		super(a, b);
		
	}

	@Override
	public void calArea() {
		
		a = 10;
		b = 5;
		float area = 0.5f*a*b;
		System.out.println("area is "+area);
		
	}

	@Override
	public void displayShapeName() {
		
		System.out.println("shape is triangle");
	}
	
}
