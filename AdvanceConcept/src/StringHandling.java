
public class StringHandling {

	public static void main(String[] args) {
		
		String s = new String("Max");
		String a = "Max";  //String literals,here Max is actual object
		String b = "   Hi Bro";
		System.out.println(a.length());
		System.out.println(b.trim());
		
		StringBuffer q = new StringBuffer("Hello");
		StringBuffer q1 = new StringBuffer(" World");
		System.out.println(q.append(q1));
		System.out.println(q);
		

	}

}
