package com.netcracker;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class ProDetal {

	public static void main(String[] args) throws IOException {
		
		Properties p = new Properties();
		
		p.setProperty("name", "Ramu");
		p.setProperty("id", "121");
		p.setProperty("salary", "123123");
		
		p.store(new FileWriter("pro.txt"), "PersonDetails");

		p.load(new FileReader("pro.txt"));
		System.out.println("name is :"+p.getProperty("name"));
		System.out.println("id is :"+p.getProperty("id"));
		System.out.println("salary is :"+p.getProperty("salary"));
	}

}
