package com.netcracker.empDetails;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EmpDetails {
	
	public String insertEmp(Employee employee) throws IOException, SQLException {
		
		String message=null;
		String query = "insert into employee values(?,?,?)";
		Connection con = DatabaseUtility.getDbConnection();
		PreparedStatement ps = con.prepareStatement(query);
		
		ps.setInt(1, employee.getEmpId());
		ps.setString(2, employee.getEmpName());
		ps.setFloat(3, employee.getEmpSal());
		int count = ps.executeUpdate();
		
		if(count == 1) {
			
			message = "employee details have been added";
		}
		DatabaseUtility.closeDbConnection();
		return message;
	}
	
	public String deleteEmp(int empId) throws IOException, SQLException {
		
		String message=null;
		String query = "delete from employee where emp_id=?";
		Connection con = DatabaseUtility.getDbConnection();
		PreparedStatement ps = con.prepareStatement(query);
		ps.setInt(1, empId);
		int count = ps.executeUpdate();
		
		if(count == 1) {
			message = "employee record has been deleted";
		}
		
		DatabaseUtility.closeDbConnection();
		return message;
	
	}


}
