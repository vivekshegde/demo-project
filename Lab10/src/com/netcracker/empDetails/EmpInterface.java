package com.netcracker.empDetails;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

public class EmpInterface {

	public static void main(String[] args) throws SQLException {
		
		System.out.println("employees details");
		
		System.out.println("1. enter the employee details");
		System.out.println("2. delete the employee details");
		System.out.println("3: exit");
		
		EmpDetails empDetails = new EmpDetails();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			
			System.out.println("enter the choice");
			try {
			int choice = Integer.parseInt(br.readLine());
			
			switch(choice) {
			
			case 1 : {
				
				Employee employee = new Employee();
				System.out.println("enter the emp id");
				int id = Integer.parseInt(br.readLine());
				System.out.println("enter the emp name");
				String name = br.readLine();
				System.out.println("enter the emp salary");
				float salary = Float.parseFloat(br.readLine());
				employee.setEmpId(id);
				employee.setEmpName(name);
				employee.setEmpSal(salary);
				String result = empDetails.insertEmp(employee);
				System.out.println(result);
				break;
			}
			case 2 :{
				//Employee employee = new Employee();
				System.out.println("enter the emp id to be deleted");
				int empId = Integer.parseInt(br.readLine());
				String result = empDetails.deleteEmp(empId);
				System.out.println(result);
				break;
			}
			
			case 3 :{
				
				System.exit(0);
			}
			default :{
				
				System.out.println("enter the valid values");
			}
				
			}
			
			}catch(IOException e) {
				
				System.out.println("enter the valid choice"+ e.getMessage());
			}
		}
	}

}
