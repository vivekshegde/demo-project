package com.netcracker.empDetails;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;



public class DatabaseUtility {

	static Connection con;
	public static Connection getDbConnection() throws IOException, SQLException {
		
		
		FileInputStream fis = new FileInputStream("jdbc.properties");
		Properties props = new Properties();
		props.load(fis);
		
		con = DriverManager.getConnection(props.getProperty("url"),props.getProperty("username"),props.getProperty("password"));
		
		return con;
	}
	
	public static void closeDbConnection() throws SQLException {
			
			con.close();
		
	}

}
