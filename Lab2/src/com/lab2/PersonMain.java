package com.lab2;

public class PersonMain {

	String firstName;
	String lastName;
	int pno;
	char gender;
	
	
	public PersonMain(String firstName, String lastName, char gender) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
	}
	
	public void setPno(int pno) {
		this.pno = pno;
	}

	void display() {
		
		System.out.println(firstName+" "+lastName+" ");
		System.out.println("gender is : "+gender);
		System.out.println("phone no is : "+pno);
	}

}
