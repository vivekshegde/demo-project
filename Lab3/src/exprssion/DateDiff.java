package exprssion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.Period;

public class DateDiff {
	
	 public static void main(String[] args) throws NumberFormatException, IOException    {
		 
		 System.out.println("enter the details");
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("enter the year");
		 int year = Integer.parseInt(br.readLine());
		 System.out.println("enter the month");
		 int month = Integer.parseInt(br.readLine());
		 System.out.println("enter the days");
		 int days = Integer.parseInt(br.readLine());
		 
	        LocalDate pdate = LocalDate.of(year, month, days);
	        LocalDate now = LocalDate.now();    //takes the present system date
	        //System.out.println(now);
	        Period diff = Period.between(pdate, now);
	 
	     System.out.printf("\nDifference is %d years, %d months and %d days old\n\n", 
	                    diff.getYears(), diff.getMonths(), diff.getDays());
	  }

}
