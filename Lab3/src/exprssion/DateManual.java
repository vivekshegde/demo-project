package exprssion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateManual {
	
	public static void dateDifference(Date date1) {
		
		Date date = new Date();
		System.out.println(date);
		
		
		long diffInMilliSec = date.getTime() - date1.getTime();
        
        long seconds = (diffInMilliSec / 1000) % 60;
         
        long minutes = (diffInMilliSec / (1000 * 60)) % 60;
         
        long hours = (diffInMilliSec / (1000 * 60 * 60)) % 24;
         
        long days = (diffInMilliSec / (1000 * 60 * 60 * 24)) % 365;
         
        long years =  (diffInMilliSec / (1000l * 60 * 60 * 24 * 365));
        
        System.out.println("difference in time is");
        
        System.out.println(years+" years, "+days+" days, "+hours+" hours, "+minutes+" minutes, "+seconds+" seconds");
		
	}

	public static void main(String[] args) throws IOException, ParseException {

		System.out.println("enter the date in dd-mm-yyyy format");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String dateValue = br.readLine();
		
		SimpleDateFormat dateFor = new SimpleDateFormat("dd-mm-yyyy");
		Date date1 = dateFor.parse(dateValue);
		
		dateDifference(date1);
	}

}
