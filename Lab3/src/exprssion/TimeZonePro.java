package exprssion;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TimeZonePro {

	public static void main(String[] args) {

		//Starting with an java.time.Instant value
	    Instant timeStamp= Instant.now();
	    System.out.println("Machine Time Now:" + timeStamp);
	 
	    //timeStamp in zone - "America/Los_Angeles"
	    ZonedDateTime LAZone= timeStamp.atZone(ZoneId.of("America/Los_Angeles"));
	    System.out.println("In Los Angeles(America) Time Zone:"+ LAZone);
	}

}
