package exprssion;

import java.util.Date;

public class PersonDetails {

	String firstName;
	String lastName;
	String gender;
	Date dob;
	
	

	public PersonDetails(String firstName, String lastName, String gender, Date dob) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dob = dob;
	}

	public PersonDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "PersonDetails [firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender + ", dob="
				+ dob + "]";
	}

	public String calculateAge(Date dateOfBirth) {
		
		Date date = new Date();
		
		long diffInMilliSec = date.getTime() - dateOfBirth.getTime();
		long days = (diffInMilliSec / (1000 * 60 * 60 * 24)) % 365;
        
        long years =  (diffInMilliSec / (1000l * 60 * 60 * 24 * 365));
        
        return years+" years" + " " +days+" days";
	}
	
	public String fullName(String firstName, String lastName) {
		
		return firstName+ " " + lastName;
	}
	
}
