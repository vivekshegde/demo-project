package exprssion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Person {

	public static void main(String[] args) throws ParseException, IOException {

		PersonDetails person = new PersonDetails();
		
		System.out.println("enter the person details");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String firstName = br.readLine();
		String lastName = br.readLine();
		String gender = br.readLine();
		
		SimpleDateFormat dateFor = new SimpleDateFormat("dd-mm-yyyy");
		Date dob = dateFor.parse(br.readLine());
		
		person.setFirstName(firstName);
		person.setLastName(lastName);
		person.setGender(gender);
		person.setDob(dob);
		
		String myAge = person.calculateAge(person.getDob());
		System.out.println("my age is "+ myAge);
		String myFullName = person.fullName(person.getFirstName(), person.getLastName());
		System.out.println("my fullName is "+ myFullName);
		System.out.println("whole my details are "+person.toString());
	}

}
