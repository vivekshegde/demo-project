package com.netcracker;

public class Account {

	public static void main(String[] args) {
		
		int availBal = 10000;
		int withdrawAmt = Integer.parseInt(args[0]);
		int Rem = availBal-withdrawAmt;
		try {
			
			if(Rem<5000) {
				
				throw new MinBalExcption("min bal should be 5000");
				
			}else {
				
				System.out.println("Remaining Balance is "+ Rem);
			}
			
		}catch(MinBalExcption e) {
			
			e.printStackTrace();
			System.out.println("Maintain min balance in your account, "+ e.getMessage());
		}

	}

}
