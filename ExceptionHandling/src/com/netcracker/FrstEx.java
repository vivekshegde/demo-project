package com.netcracker;

public class FrstEx {

	public static void main(String[] args)  {
		
		int x = 20;
		int res = 0;
		int num = 0;
		try {
			num = Integer.parseInt(args[0]);
			if(num==0) {
				
				throw new ArithmeticException("number value should not be zero");
			
			}else {
			
				res = x/num;  //new ArithmeticException();
			}
		
		}catch(ArithmeticException e) {
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			System.out.println("exception found " + e.getMessage());
			
		}
		
		finally {
		System.out.println("x value is "+x);
		System.out.println("res value is "+res);
		System.out.println("num value is "+num);

		}
	}

}
