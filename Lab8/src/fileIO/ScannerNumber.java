package fileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerNumber {

	public static void main(String[] args) throws FileNotFoundException {

		Scanner sc = new Scanner(new File("number.txt")).useDelimiter("\\,");
		
		System.out.println("orginal numbers are ");
		while(sc.hasNextInt()) {
			
			System.out.print(sc.nextInt()+" ");
			int i = sc.nextInt();
		}
	}

}
