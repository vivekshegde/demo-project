package fileIO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;


public class ReadWrite {
	

	public static void main(String[] args) throws IOException {

		String reverse ="";
		String rline="";
		System.out.println(" the content of the file");
		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		//String line = br.readLine();
		//writeFile(line);
		FileReader fr = new FileReader("demopage.txt");
		BufferedReader br = new BufferedReader(fr);
		
		String line;
		
		
		while((line=br.readLine())!=null) {
			
			System.out.println("content of this file is "+line);
			rline = rline + line;
		}
		
		
		for(int i=rline.length()-1;i>=0;i--) {
			
			reverse = reverse+rline.charAt(i);
		}
		System.out.println("reverse string is "+reverse);
		
		
		FileWriter fw = new FileWriter("demoPage1.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		//String fileContent = "This is a sample text.";
		bw.write(reverse);
		
		bw.close();
		fw.close();
		br.close();
		fr.close();
		
		
	}

}
